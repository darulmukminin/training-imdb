import React, { Component } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap'

export class ModalTrailer extends Component {
    constructor(props) {
        super()
        this.state = {
            view: props.view
        }
    }

    componentDidUpdate(oldProps, oldState) {
        if (oldProps.view != this.props.view) {
            this.setState({
                ...this.state,
                view: this.props.view
            })
        }
    }

    onClose = () => {
        this.props.onClose()
    }


    render() {
        return (
            <div>
                <Modal size='xl' show={this.state.view}  >
                    <Modal.Header style={{ backgroundColor: "grey" }}>
                        <Modal.Title style={{ color: "white", fontSize: "30px", }} >Trailer {this.props.tittle}</Modal.Title>
                        <Button onClick={() => this.onClose()} variant='danger'>X</Button>
                    </Modal.Header>
                    <Modal.Body style={{ backgroundColor: "grey" }}>
                        <Row>
                            <iframe width="420" height="420" src={"https://www.youtube.com/embed/" + this.props.trailer}>
                            </iframe>
                        </Row>

                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

export default ModalTrailer
