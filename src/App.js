import './App.css';
import { Route, Routes } from 'react-router-dom';
import { RoutingPath } from './utils/globalVariable';

import Signin from './components/signin/signin';
import Landing from './components/landingpage/landing';
import Whistlist from './components/whistlist/whistlist';

function App() {

  return (
    <div className="App">
      <Routes>
        <Route path='/*' element={<Landing />} />
        <Route path={RoutingPath.signin} element={<Signin />} />
        <Route path={RoutingPath.landing} element={<Landing />} />
        <Route path={RoutingPath.whistlist} element={<Whistlist />} />
      </Routes>
    </div>
  );
}

export default App;
