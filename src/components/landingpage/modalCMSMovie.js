import React, { Component } from 'react'
import { Button, Col, Container, Form, Modal, Row } from 'react-bootstrap'
import { post } from '../../utils/globalFunction'
import { Endpoint } from '../../utils/globalVariable'

export class ModalCMSMovie extends Component {

    constructor(props) {
        super()
        this.state = {
            view: props.view,
            ID: '',
            tittle: '',
            rating: '',
            images: '',
            trailer: '',
            param: 'ADD',
            displayDelete: 'none',
        }
    }

    componentDidUpdate(oldProps, oldState) {
        if (oldProps.view != this.props.view) {
            this.setState({
                ...this.state,
                view: this.props.view,
                param: this.props.param,
            })
            if (this.props.param == "EDIT" || this.props.param == "DELETE") {
                this.setState({
                    ...this.state,
                    view: this.props.view,
                    ID: this.props.data.ID,
                    tittle: this.props.data.tittle,
                    rating: this.props.data.rating,
                    images: this.props.data.images,
                    trailer: "https://www.youtube.com/watch?v=" + this.props.data.trailer,
                    param: this.props.param,
                    displayDelete: '',
                })
            }
        }
    }




    setValue = (e, pstate) => {
        this.setState({
            ...this.state,
            [pstate]: e
        })
    }

    onSubmit = (e) => {
        if (window.confirm("Yakin ingin memproses ?")) {
            switch (this.state.param) {
                case "ADD":
                    this.create()
                    break;
                case "EDIT":
                    this.edit()
                    break;
                case "DELETE":
                    this.delete()
                    break;

                default:
                    break;
            }
        }
    }
    onDelete = (e) => {
        if (window.confirm("Yakin ingin memproses ?")) {
            this.delete()
        }
    }

    create = async () => {
        var body = {
            tittle: this.state.tittle,
            rating: this.state.rating,
            images: this.state.images,
            trailer: this.state.trailer,
        }
        var response = await post(Endpoint.addMovie, body)
        if (response.response == '00') {
            this.props.onClose()
        } else {
            alert(response.message)
        }
    }
    edit = async () => {
        var body = {
            ID: this.state.ID,
            tittle: this.state.tittle,
            rating: this.state.rating,
            images: this.state.images,
            trailer: this.state.trailer,
        }
        var response = await post(Endpoint.editMovie, body)
        if (response.response == '00') {
            this.props.onClose()
        } else {
            alert(response.message)
        }
    }
    delete = async () => {
        var body = {
            ID: this.state.ID,
            tittle: this.state.tittle,
        }
        var response = await post(Endpoint.deleteMovie, body)
        if (response.response == '00') {
            this.props.onClose()
        } else {
            alert(response.message)
        }
    }

    render() {
        return (
            <div>
                <Modal size='xl' show={this.state.view} >
                    <Modal.Header>
                        <Modal.Title style={{ fontSize: "30px" }} >{this.props.param} MOVIE</Modal.Title>
                        <Button onClick={() => this.props.onClose()} >X</Button>
                    </Modal.Header>
                    <Modal.Body>
                        <Container>
                            <Row>
                                <Form.Group>
                                    <Form.Label>Tittle</Form.Label>
                                    <Form.Control value={this.state.tittle} placeholder="Enter Tittle"
                                        onChange={(e) => this.setValue(e.target.value, "tittle")}
                                    ></Form.Control>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Rating</Form.Label>
                                    <Form.Control value={this.state.rating} placeholder="Enter Rating"
                                        onChange={(e) => this.setValue(e.target.value, "rating")}></Form.Control>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Images</Form.Label>
                                    <Form.Control value={this.state.images} placeholder="Enter images"
                                        onChange={(e) => this.setValue(e.target.value, "images")}></Form.Control>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Trailer</Form.Label>
                                    <Form.Control value={this.state.trailer} placeholder="Enter trailer"
                                        onChange={(e) => this.setValue(e.target.value, "trailer")}></Form.Control>
                                </Form.Group>

                            </Row>
                        </Container>
                    </Modal.Body>
                    <Modal.Footer>
                        <Row>
                            <Col style={{ display: [this.state.displayDelete] }} >
                                <Button variant="danger" onClick={(e) => this.onDelete(e)} >DELETE</Button>
                            </Col>
                            <Col>
                                <Button onClick={(e) => this.onSubmit(e)}>SAVE</Button>
                            </Col>
                        </Row>

                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default ModalCMSMovie