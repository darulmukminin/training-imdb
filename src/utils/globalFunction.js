import { Endpoint } from "./globalVariable"

class GlobalFunction {
    test_fungsi = (e,max) => {
        if (e.length > max) {
            return ("panjang karakter maksimal adalah "+max)
        } else {
            return true
        }
    }

    test (e,max){

    }

}
export default new GlobalFunction

export function goTo (path) {
    window.location.href = path
}

export async function get(enpoint, parameter) {
    let url = new URL(Endpoint.ip+enpoint+parameter)
    let response = await fetch(url,
        {
            method:"GET",
            headers: {
                "Content-Type":"application/json"
            },
        }).then( async (response)=>{
            if (response.status === 200){
                response = await response.text()
                response = JSON.parse(response)
                return response
            } else {
                alert(response)
            }
        }).catch((error) => {
            alert(error)
            return error
        } )

    return response
}

export async function post(enpoint, body) {
    let url = new URL(Endpoint.ip+enpoint)
    let response = await fetch(url,
        {
            method:"POST",
            headers: {
                "Content-Type":"application/json",
            },
            body : JSON.stringify(body)
        }).then( async (response)=>{
            if (response.status === 200){
                response = await response.text()
                response = JSON.parse(response)
                return response
            } else {
                alert(response)
            }
        }).catch((error) => {
            alert(error)
            return error
        } )

    return response
}