import React, { Component } from 'react';
import './signin.css'
import { Button, Form, Modal } from 'react-bootstrap';
import { Endpoint, parameter, RoutingPath } from '../../utils/globalVariable';
import GlobalFunction, { get, goTo,post } from '../../utils/globalFunction';

export class Signin extends Component {
    constructor() {
        super()
        this.state = {
            msgValidasiEmail: '',
            msgValidasiPassword: '',
            modalSignin : false,
            value : {
                email : '',
                password : '',
                passwordSignin : '',
            }
        }
    }

    cek_karakter = (e, max,pstate,pstatevalue) => {
        var temp = GlobalFunction.test_fungsi(e, max)
        if (temp != true) {
            this.setState({
                ...this.state,
                [pstate]: temp
            })
            this.seValue(e,pstatevalue)
            
        } else {
            this.setState({
                ...this.state,
                [pstate]: ''
            })
            this.seValue(e,pstatevalue)
        }
        console.log(this.state.value.email);
    }

    seValue = (e,pstate) => {
        this.setState({
            ...this.state,
            value : {
                ...this.state.value,
                [pstate] : e
            }
        })
    }

    setModal = (pstate) => {
        this.setState({
            ...this.state,
            [pstate] : !this.state[pstate]
        })
    }

    submitSignin = async () => {
        if(this.state.value.email == '' ||this.state.value.passwordSignin == ''){
            alert("isi form terlebih dahulu")
        }else {
            var parameter = "?user=" + this.state.value.email +"&password=" + this.state.value.passwordSignin
            var response = await get(Endpoint.signin,parameter)
            if(response.response === '00'){
                localStorage.setItem('token',response.token)
                goTo(RoutingPath.landing);
            }else {
                alert(response.message)
            }

        }

    }

    submitSignup = async () => {
        if(this.state.value.email == '' ||this.state.value.password == ''){
            alert("isi form terlebih dahulu")
        }else {
            var body = {
                user:this.state.value.email,
                password:this.state.value.password
            }
            var response = await post(Endpoint.signup,body)
            if(response.response === '00'){
                this.setModal('modalSignin')
                alert(response.message)
            }else {
                alert(response.message)
            }

        }

    }


    render() {
        return <div className='background'>
            <Modal show={this.state.modalSignin}>
                <Modal.Header>
                    <Modal.Title>{parameter.tittle}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>
                        <Form>
                            <Form.Group>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control value = {this.state.value.email} type="email" placeholder="Enter email" onChange={(e) => this.cek_karakter(e.target.value, 15,"msgValidasiEmail","email")} />
                                <Form.Text className="text-muted">
                                    {this.state.msgValidasiEmail}
                                </Form.Text>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Password</Form.Label>
                                <Form.Control  value = {this.state.value.password} type="password" placeholder="Password" onChange={(e) => this.cek_karakter(e.target.value, 6,"msgValidasiPassword","password")} />
                                <Form.Text className="text-muted">
                                    {this.state.msgValidasiPassword}
                                </Form.Text>
                            </Form.Group>
                        </Form>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button  onClick = {()=>this.setModal('modalSignin')}  variant="secondary">
                        Cancel
                    </Button>
                    <Button onClick = {() => this.submitSignup()} variant="primary">
                        Save
                    </Button>
                </Modal.Footer>
            </Modal>
            <div className='container-signin' >
                <div className='container-images' >
                    <img className='image-logo' src= {parameter.icon} />
                </div>
                <input onChange={(e) => this.seValue(e.target.value,'email')} value = {this.state.value.email} className='input-signin col-11' placeholder="Username"></input>
                <input onChange={(e) => this.seValue(e.target.value,'passwordSignin')} value = {this.state.value.passwordSignin} type='password' className='input-signin col-11' placeholder="Password"></input>
                <br />
                <Button onClick={() => this.submitSignin()} > Sign-In </Button>
                <p className="col-11" 
                style={{ textAlign: "right", fontSize: "12px", cursor: "pointer", color: "blue", marginTop: "10px" }}  
                onClick = {()=>this.setModal('modalSignin')} >Create New Account</p>

            </div>
        </div>;
    }
}

export default Signin;
