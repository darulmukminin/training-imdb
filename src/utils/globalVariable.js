export const parameter = {
    tittle : "Cretae new account",
    icon : "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/IMDb_Logo_Square.svg/2048px-IMDb_Logo_Square.svg.png"
}
export const RoutingPath = {
    signin : "/signin",
    landing : "/landing",
    whistlist : "/whistlist"
}
export const Endpoint = {
    ip:"http://localhost:3030",
    signin:"/signin",
    signup:"/signup",
    newMovie:"/newmovie",
    favMovie:"/favmovie",
    carousel:"/carousel",
    search:"/search",
    addMovie:"/addmovie",
    editMovie:"/editmovie",
    deleteMovie:"/deletemovie",
}