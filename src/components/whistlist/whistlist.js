import React, { Component } from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import { goTo } from '../../utils/globalFunction'
import { parameter, RoutingPath } from '../../utils/globalVariable'
import './whistlist.css'

export class Whistlist extends Component {
    render() {
        return (<div className='background-land'>
            <Container>
                {/* top bar */}
                <Row className='topbar' >
                    <Col lg={11} xs={12} >
                        <img className='image-logo-landing' src={parameter.icon} onClick = {()=>goTo(RoutingPath.landing) } />
                    </Col>
                    <Col lg={1} xs={12} style={{ textAlign: 'left' }} >
                        <Button style={{ margin: "1px", width: "100px" }} className='button-profile' onClick = {()=>goTo(RoutingPath.signin) } >Signin</Button>
                    </Col>
                </Row>
                {/* body card */}
                <Row>

                    <Row>
                        <p className='text-title'>Whistlist</p>
                    </Row>
                    <Row className='flex-row flex-nowrap overflow-auto'>
                    </Row>

                </Row>
                {/* footer */}
                <Row>
                    <Row>
                        <Col>
                            <img className='footer-icon' />
                        </Col>
                    </Row>
                    <Row>
                        <Col className='footer-text'>
                            1990-2022 by IMDb
                        </Col>
                    </Row>

                </Row>
            </Container>
        </div>
        )
    }
}

export default Whistlist