import React, { Component } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import './landing.css'
import ModalCMSMovie from './modalCMSMovie';
import ModalDetail from './modalDetail';
import ModalTrailer from './modalTrailer';

export class Cardview extends Component {
    constructor() {
        super()
        this.state = {
            modalCMSMovie:false,
            modalDetail: false,
            modalTrailer: false
        }
    }

    setModal = (pstate) => {
        this.setState({
            ...this.state,
            [pstate]: !this.state[pstate]
        })
    }

    onWistlist = () => {
        console.log(this.props.list);
    }
    
    render() {
        return <div className='cardview-body' >
             <ModalCMSMovie view={this.state.modalCMSMovie} param = "EDIT" data = {this.props.list} onClose = {() => this.setModal('modalCMSMovie')} ></ModalCMSMovie>
            <ModalDetail
                view={this.state.modalDetail}
                tittle={this.props.tittle}
                rating = {this.props.rating}
                image={this.props.image}
                onClose={() => this.setModal('modalDetail')} />
            <ModalTrailer
                view={this.state.modalTrailer}
                tittle={this.props.tittle}
                trailer = {this.props.trailer}
                onClose={() => this.setModal('modalTrailer')} />
            <Container style = {{paddingLeft:'0px',paddingRight:'0px'}} >
                <div onClick={() => this.setModal('modalDetail')} style={{cursor:"pointer"}} >

                    <Row>
                        <img className='cardview-images' src={this.props.image} />
                    </Row>
                    <Row>
                        <Col xs={2} md={2} lg={2} >
                            <img className='cardview-icon' src='https://www.freepnglogos.com/uploads/star-png/star-icon-small-flat-iconset-paomedia-0.png' />
                        </Col>
                        <Col xs={8} md={8} lg={8}>
                            <p className='cardview-rating'>{this.props.rating}</p>
                        </Col>
                        <Col>
                        <p className='cardview-tittle'>{this.props.tittle}</p>
                        </Col>
                    </Row>
                </div>
            </Container>
            <Row>
                <Col>
                <Button className='cardview-button' onClick={() => this.setModal('modalTrailer')}>Trailer</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                <Button variant='danger' className='cardview-button' onClick={() => this.onWistlist()}>+</Button>
                </Col>
                <Col>
                <Button variant='warning' className='cardview-button' onClick={() => this.setModal('modalCMSMovie')}>Edit</Button>
                </Col>
            </Row>
        </div>;
    }
}

export default Cardview;
