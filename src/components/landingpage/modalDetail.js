import React, { Component } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap'

export class ModalDetail extends Component {
    constructor(props) {
        super()
        this.state = {
            view: props.view
        }
    }

    componentDidUpdate(oldProps, oldState) {
        if (oldProps.view != this.props.view) {
            this.setState({
                ...this.state,
                view: this.props.view
            })
        }
    }

    onClose = () => {
        this.props.onClose()
    }


    render() {
        return (
            <div>
                <Modal size='xl' show={this.state.view}  >
                    <Modal.Header style={{ backgroundColor: "grey" }}>
                        <Modal.Title style={{ color: "white", fontSize: "30px", }} >Sinopsis</Modal.Title>
                        <Button onClick={() => this.onClose()} variant='danger'>X</Button>
                    </Modal.Header>
                    <Modal.Body style={{ backgroundColor: "grey" }}>
                        <Row>
                            <Col lg={5} >
                                <img style={{ width: "80%" }} src={this.props.image} ></img>
                            </Col>
                            <Col lg={7} >
                                <Row>
                                    <Col>
                                        <p style={{ color: "white", fontSize: "35px", textAlign: "left" }} >{this.props.tittle}</p>
                                    </Col>
                                    <Col>
                                        <p style={{ color: "yellow", fontSize: "35px", textAlign: "right" }} >{this.props.rating}/100</p>
                                    </Col>
                                </Row>
                                <Col>
                                    <p style={{ color: "white", fontSize: "25px", textAlign: "justify", textIndent: "0.5in" }} >
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsu
                                    </p>
                                </Col>
                            </Col>
                        </Row>

                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

export default ModalDetail
