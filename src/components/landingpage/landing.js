import React, { Component } from 'react';
import { Button, Carousel, Col, Container, Form, Row } from 'react-bootstrap';
import { Endpoint, parameter, RoutingPath } from '../../utils/globalVariable';
import './landing.css'
import Cardview from './cardview'
import { get, goTo } from '../../utils/globalFunction';
import { newMovie } from './dummy';
import { BsFillBookmarkHeartFill, BsSearch } from "react-icons/bs";
import ModalCMSMovie from './modalCMSMovie';

export class Landing extends Component {
    constructor() {
        super()
        this.state = {
            modalCMSMovie: false,
            componentSearchMovie: [],
            componentCarousel: [],
            componentNewMovie: [],
            componentFavMovie: [],
            componentPencarian: 'none',
            countWistlist: '',
            valueSearch: '',

        }
    }

    componentDidMount() {
        this.setCarousel()
        this.setNewMovie()
        this.setFavMovie()
    }

    setSearch = async () => {
        var response = await get(Endpoint.search, '?search='+this.state.valueSearch)
        var setComponent = []
        if (response.response == '00') {
            response.data.forEach((data, i) => {
                setComponent.push(<Cardview key={i} list={data} tittle={data.tittle} rating={data.rating} image={data.images} trailer={data.trailer} />)
            });

        }
        this.setState({
            ...this.state,
            componentSearchMovie: setComponent
        })
        this.setstate('componentPencarian','')
    }

    setNewMovie = async () => {
        var response = await get(Endpoint.newMovie, '')
        var setComponent = []
        if (response.response == '00') {
            response.data.forEach((data, i) => {
                setComponent.push(<Cardview key={i} list={data} tittle={data.tittle} rating={data.rating} image={data.images} trailer={data.trailer} />)
            });

        }
        this.setState({
            ...this.state,
            componentNewMovie: setComponent
        })
    }
    setFavMovie = async () => {
        var response = await get(Endpoint.favMovie, '')
        var setComponent = []
        if (response.response == '00') {
            response.data.forEach((data, i) => {
                setComponent.push(<Cardview key={i} list={data} tittle={data.tittle} rating={data.rating} image={data.images} trailer={data.trailer} />)
            });

        }
        this.setState({
            ...this.state,
            componentFavMovie: setComponent
        })
    }

    setstate = (state, value) => {
        this.setState({
            ...this.state,
            [state]: value
        })
    }

    onSearch = (e) => {
        if (e == "") {
            this.setstate('componentPencarian', 'none')
        } else {
            this.setstate('valueSearch', e)
        }
    }

    setCarousel = async () => {
        var response = await get(Endpoint.carousel, '')
        var setComponent = []
        if (response.response == '00') {
            let intv = 0
            response.data.forEach((data, i) => {
                intv = + 1500
                setComponent.push(
                    <Carousel.Item key={i} interval={intv}>
                        <img
                            className="d-block w-100"
                            src={data.images}
                        />
                        <Carousel.Caption>
                            <h3 style={{ strokeWidth: "2" }} >{data.tittle}</h3>
                            <p></p>
                        </Carousel.Caption>
                    </Carousel.Item>
                )

            })
        }
        this.setState({
            ...this.state,
            componentCarousel: setComponent
        })
    }

    setModal = (pstate) =>{
        this.setState({
            ...this.state,
            [pstate]: !this.state[pstate]
        })
    }


    render() {
        return <div className='background-land'>
            <ModalCMSMovie view={this.state.modalCMSMovie} param = "ADD" onClose = {() => this.setModal('modalCMSMovie')} ></ModalCMSMovie>
            <Container>
                {/* top bar */}
                <Row className='topbar' >
                    <Col lg={1} xs={12} >
                        <img className='image-logo-landing' src={parameter.icon} />
                    </Col>
                    <Col lg={8} xs={10} >
                        <Form.Group>
                            <Form.Control type="text" placeholder="Search" onChange={(e) => this.onSearch(e.target.value)} />
                        </Form.Group>
                    </Col>
                    <Col lg={2} xs={2} style={{ textAlign: 'left' }} >
                        <Button style={{ marginRight: "10px", width: "70px" }} onClick={() => this.setSearch()}><BsSearch /></Button>
                        <Button style={{ marginRight: "1px" }} variant='danger' onClick={() => goTo(RoutingPath.whistlist)} ><BsFillBookmarkHeartFill /> {this.state.countWistlist}</Button>
                    </Col>
                    <Col lg={1} xs={2} style={{ textAlign: 'left' }} >
                        <Button style={{ margin: "1px", width: "100px" }} className='button-profile' onClick={() => goTo(RoutingPath.signin)} >Signin</Button>
                    </Col>
                </Row>

                <Row style={{ display: [this.state.componentPencarian] }} >

                    <Row>
                        <p className='text-title'>Hasil Pencarian</p>
                    </Row>
                    <Row className='flex-row flex-nowrap overflow-auto'>
                        {this.state.componentSearchMovie}
                    </Row>

                </Row>

                {/* carousel */}
                <Row>
                    <Carousel>
                        {this.state.componentCarousel}
                    </Carousel>
                </Row>
                {/* body card */}
                <Row>
                    <Col>
                    <Button style = {{marginTop:"20px",width:"70%"}} onClick = {()=>this.setModal('modalCMSMovie')} >Add Movie</Button>
                    </Col>
                </Row>

                <Row>

                    <Row>
                        <p className='text-title'>New Movie</p>
                    </Row>
                    <Row className='flex-row flex-nowrap overflow-auto'>
                        {this.state.componentNewMovie}
                    </Row>

                </Row>
                <Row>

                    <Row>
                        <p className='text-title'>Favorite Movie</p>
                    </Row>
                    <Row className='flex-row flex-nowrap overflow-auto'>
                    {this.state.componentFavMovie}
                    </Row>
                </Row>
                {/* footer */}
                <Row>
                    <Row>
                        <Col>
                            <img className='footer-icon' />
                        </Col>
                    </Row>
                    <Row>
                        <Col className='footer-text'>
                            1990-2022 by IMDb
                        </Col>
                    </Row>

                </Row>
            </Container>
        </div>;
    }
}

export default Landing;
